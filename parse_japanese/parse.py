# -*- coding: utf-8 -*-

import yomi_dict

from anki.hooks import addHook
from aqt import mw
from aqt.addcards import AddCards
from aqt.qt import *
from aqt.utils import showInfo
#from aqt import mw QObject
#from aqt import mw pyqtSlot
#from aqt import mw pyqtProperty
from anki.hooks import wrap
raw_expression_field = 'RawExpression'
expression_field = 'Expression'
note_type_name = "jparser"
vocab_deck = "Vocabulary Deck"
vocab_model = "Vocabulary"

class Parser:
    def __init__(self):
        self.dict = yomi_dict.initLanguage()  #To be changed
    def color_notes(self, expression):
        if not expression:
            return 'black'
        deck_id = mw.col.decks.id(vocab_deck)
        notes_id=mw.col.findNotes("")
        for note_id in notes_id:
            note = mw.col.getNote(note_id)
            if expression in note.fields:
                cards_id=mw.col.findCards("nid:"+str(note_id))
                if len(cards_id) == 0:
                    continue
                for card_id in cards_id:
                    card = mw.col.getCard(card_id)
                    if card.did == deck_id:
                        if card.queue==2:
                            return 'green'
                        elif card.queue==-1:
                            return '#dbc254'
                        else:
                            return 'black'
        return 'red'
    def parse(self, raw_expression):
        expression = ''
        explanation=''
        jsFunction='<script>function hoverDiv(e,divId){var left=e.clientX+"px";var top=e.clientY+"px";var div=document.getElementById(divId);div.style.left=left;div.style.top=top;div.style.display=\'inline-block\';return false;}</script>'
        jsFunction+='<script>function addCard(exp,read,glos,divId){alert("weszlo do srodka");if(document.getElementById(divId).style.color=="red"){alert("jest w funkcji");pluginObject.add_vocab_note(exp,read,glos);}}</script>'
        i = 0
        while i < len(raw_expression):
            meanings = self.dict.findTerm(raw_expression[i:])
            if meanings and meanings[1]>0:
                meaning = meanings[0][0]
                if(meaning['source'] != raw_expression[i:i+len(meaning['source'])]):
                    expression += raw_expression[i]
                    i += 1
                    continue
                '''
                expression += '<span title="'
                expression += (meaning['source'])
                expression += "\n"
                if(meaning['source']!=meaning['reading']):
                    expression += (meaning['reading'])
                    expression += "\n"
                expression += (meaning['glossary'])
                expression += '">'
                expression +='<font color="'

                #expression +=self.color_notes(meaning['source'])
                expression+=self.color_notes(meaning['source'])

                #print(mw.col.getNote(tr[0]))

                expression +='">'
                expression += meaning['source']
                expression +='</font>'
                expression += '</span>'
                '''
                #expression+='
                expression +='<span id="'+meaning['glossary']+'" style="postion:absolute;color:'+self.color_notes(meaning['source'])+'"  onmouseover=\'hoverDiv(event,"'+meaning['source']+'")\''
                #expression += 'document.getElementById("'+meaning['source']+'").style.display='
                #expression += '"block"\''
                expression += 'onClick=\'addCard("'+meaning['source']+'","'+meaning['reading']+'","'+meaning['glossary']+'","'+meaning['glossary']+'")\' onmouseout=\' document.getElementById("'+meaning['source']+'").style.display="none"\'>'
                #expression +='<font color="'

                #expression +=self.color_notes(meaning['source'])
                #expression +=self.color_notes(meaning['source'])
                #expression +='">'
                expression += meaning['source']
                expression +='</font>'
                expression += '</span>'

                explanation +='<div id="'+meaning['source']+ '" style="display:none;position:fixed;z-index:1;background-color:#dedcde;border:1px solid #000;width:100px;height;100px;font-size:15px;" onmouseout=\' document.getElementById("'+meaning['source']+'").style.display="none"\' onmouseover=\'hoverDiv(event,"'+meaning['source']+'")\'>'
                explanation += meaning['source']
                explanation += "<br>"
                if(meaning['source']!=meaning['reading'] and meaning['reading']):
                    explanation += (meaning['reading'])
                    explanation += "<br>"
                explanation += (meaning['glossary'])
                explanation += '</div>'

                if(meaning['source'] > 0):
                    i += len(meaning['source'])
                else:
                    expression += raw_expression[i]
                    i += 1
            else:
                expression += raw_expression[i]
                i += 1
        return (jsFunction+expression+explanation)

def focus_lost(flag, note, fidx):
    if not parser:
        return flag

    if note_type_name not in note.model()['name'].lower():
        return flag

    #check whether the event comes from the source field
    if fidx != mw.col.models.fieldNames(note.model()).index(raw_expression_field):
        return flag

    if update_note(note):
        return True

    return flag

def rendering(txt, extra, context,tag,tag_name):

    return parser.parse(txt)

def update_note(note):
    """
    :param note: note to be checked whether meaning and reading needs an update
    :return: True if updated, False if not updated
    """
    if note_type_name not in note.model()['name'].lower():
        return False
    if not raw_expression_field in note:
        return False

    if not expression_field in note:
        return False

    text = mw.col.media.strip(note[raw_expression_field])

    if not text.strip():
        return False

    expression = parser.parse(text)
    note[expression_field] = expression
    return True
class javascriptObject(QObject):
    #_dupa='tak'

    #dupa=pyqtProperty(str,lambda self:self._dupa)
    @pyqtSlot(str,str,str)
    def add_vocab_note(self,expression, hiragana, gloss):
        deck_id = mw.col.decks.id(vocab_deck)
        if expression == '':
            expression = hiragana
        if expression == hiragana:
            hiragana = ''
        if vocab_model in mw.col.models.allNames():
            model = mw.col.models.byName(vocab_model)
            mw.col.models.update(model)
            mw.col.models.setCurrent(model)
            mw.col.models.save(model)
        else:
            model = mw.col.models.new(vocab_model)
            exp_field = mw.col.models.newField("Expression")
            hira_field = mw.col.models.newField("Hiragana")
            gloss_field = mw.col.models.newField("Glossary")
            mw.col.models.addField(model, exp_field)
            mw.col.models.addField(model, hira_field)
            mw.col.models.addField(model, gloss_field)

            template = mw.col.models.newTemplate("Default")
            template['qfmt'] = '{{Expression}}'
            template['afmt'] = '{{FrontSide}}\n\n<hr id=answer>\n\n{{Hiragana}}<br/>{{Glossary}}'
            template['did'] = deck_id
            mw.col.models.addTemplate(model, template)
            mw.col.genCards([])
            mw.col.models.add(model)
        deck = mw.col.decks.get(deck_id)
        deck['mid'] = mw.col.models.byName(vocab_model)['id']
        mw.col.decks.update(deck)
        mw.col.decks.select(deck_id)

        panel = mw.add_vocab = AddCards(mw)
        note = panel.editor.note
        note['Expression'] = expression
        note['Hiragana'] = hiragana
        note['Glossary'] = gloss
        panel.editor.setNote(note)
        panel.show()

jsObj=javascriptObject()
flag=None
def _initWeb():
    global flag
    flag=True

def _showQuestion():
    global jsObj,flag
    if flag:
        flag=False
        mw.web.page().mainFrame().addToJavaScriptWindowObject("pluginObject",jsObj)

mw.reviewer._initWeb=wrap(mw.reviewer._initWeb,_initWeb,"before")
mw.reviewer._showQuestion=wrap(mw.reviewer._showQuestion,_showQuestion,"before")

parser = Parser()
#addHook('editFocusLost', focus_lost)
addHook("fmod_jparser",rendering)
#addHook("browser.setupMenus", setup_menu_item)
if __name__ == "__main__":
    #examples are shamelessly taken from Japanese Support Anki Plugin
    expr = u"カリン、自分でまいた種は自分で刈り取れ"
    print (parser.parse(expr).encode("utf-8"))
    expr = u"昨日、林檎を2個買った。"
    print (parser.parse(expr).encode("utf-8"))
    expr = u"真莉、大好きだよん＾＾"
    print (parser.parse(expr).encode("utf-8"))
    expr = u"彼２０００万も使った。"
    print (parser.parse(expr).encode("utf-8"))
    expr = u"彼二千三百六十円も使った。"
    print (parser.parse(expr).encode("utf-8"))
    expr = u"千葉"
    print (parser.parse(expr).encode("utf-8"))
