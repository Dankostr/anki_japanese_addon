# -*- coding: utf-8 -*-

import yomi_dict
import sys
import operator

from anki.hooks import addHook
from aqt import mw
from aqt.addcards import AddCards
from aqt.qt import *
from aqt.utils import showInfo

class DueWindow(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        print(mw)
        mw.myWidget=self
        self.setWindowTitle("Change due")
        table=QTableView(self)
        header=['text','deck','due','id']
        self.model=Model(self.get_data(),header,self)
        table.setModel(self.model)
        #table.setShowGrid(False)
        table.setMinimumSize(300,300)
        table.verticalHeader().setVisible(False)
        table.horizontalHeader().setStretchLastSection(True)
        table.resizeColumnsToContents()
        table.resizeRowsToContents()
        table.setColumnHidden(3,True)
        table.setSortingEnabled(True)
        self.layout=QVBoxLayout()
        self.layout.addWidget(table)
        self.setLayout(self.layout)

        table.clicked.connect(self.set_due)
        self.show()

    def set_due(self,index):
        if(index.column()==2):
            input,result=QInputDialog.getText(self,"Give new due","Give new due")
            if result==True:
                try:
                    if(int(input)<0):
                        raise Exception()

                    name=self.model.index(index.row(),3)
                    mw.col.db.executescript("update cards set due="+input+" where id="+str(name.data()))
                    suc=self.model.setData(index,input)
                    #mw.col.findNotes(nazwa po lewej)
                    #mw.col.findCards(nid z wyzej)
                    #cardid=mw.col.getCard(z wyzej [0])
                    #mw.col.db <- musze znalezc jak zrobic update ale bedzie to wygladalo
                    #tak (update cards set due=input where id=cardid )
                    #dokoncze jak wstane
                except:
                    msg=QMessageBox()
                    msg.setIcon(QMessageBox.Critical)
                    msg.setText("You have to put positive integer number ")
                    msg.setWindowTitle("Error in changing due.")
                    msg.exec_()

    def get_data(self):
        #query ;(
        listOfCardsId=(mw.col.db.list("select id from cards"))
        data=[]
        for a in listOfCardsId:
            info=[]
            noteid=mw.col.getCard(a).nid
            note=mw.col.getNote(noteid)
            info.append(str(" ").join(note.fields))
            info.append(mw.col.decks.name(mw.col.getCard(a).did))
            #info.append(note.fields)
            info.append(mw.col.getCard(a).due)
            info.append(a)
            data.append(info)
        #for a in listOfCardsId:
        #    print(mw.col.getCard(a))
        print(data)
        return  data


class Model(QAbstractTableModel):
    def __init__(self,data,header,parent=None):
        QAbstractTableModel.__init__(self,parent)
        self.arraydata=data
        self.headerdata=header
    def rowCount(self,parent):
        return len(self.arraydata)
    def columnCount(self,parent):
        if(len(self.arraydata)>0):
            return len(self.arraydata[0])
        return 0
    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.headerdata[col]
        return None
    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != Qt.DisplayRole:
            return None
        return self.arraydata[index.row()][index.column()]
    def setData(self, index, value, role=2):
        if role==2:
            row=index.row()
            column=index.column()
            self.arraydata[row][column]=value
            self.dataChanged.emit(index,index)
            return True
        return False
    def sort(self,col_number,order):
        self.emit(SIGNAL("layoutAboutToBeChanged()"))
        self.arraydata=sorted(self.arraydata,key=operator.itemgetter(col_number))
        if order==Qt.DescendingOrder:
            self.arraydata.reverse()
        self.emit(SIGNAL("layoutChanged()"))
def make_window():
    window=DueWindow()
action=QAction("Change due",mw)
action.triggered.connect(make_window)
mw.form.menuEdit.addAction(action)
